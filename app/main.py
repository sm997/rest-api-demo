from typing import Union

from fastapi import FastAPI

import os
import datetime

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/uptime")
def return_uptime():
    awake_since = os.popen('uptime -p').read()
    right_now = datetime.datetime.now()
    return {"server_uptime": awake_since,
            "current_timestamp": right_now}

@app.post("/polypeptide/{peptide_string}")
def read_peptide(peptide: Union[str, None] = None):
    return {"peptide!!!!": peptide}


